# CNO roles and platform-runner

CNO comprises various components. These components are automatically installed and configured, by using Ansible for the deployment of Helm charts, K8s manifests, REST calls and other miscellaneous tasks.

The installation and configuration for each CNO component is organized into a separate `Ansible role`. Each role installs a single component in its own namespace in a K8s cluster. Helm charts already provide a solid base for deploying K8s applications and are used extensively. The CNO roles provide an extra abstraction layer on top of these charts to simplify installation and maintenance, thereby following best practices.

There are several ways to deploy a CNO role:

- Deploy a role directly from GitLab. This is the preferred method for embedding a role in your own CI/CD pipeline.
- Download and deploy the role from source code. This is the preferred method when developing/testing a role at your own PC.

In both cases, CNO roles are deployed with the help of `ansible-playbook`. This playbook can be run in a docker container, or directly on your host machine.

This page provides some tutorials to get started with deploying CNO roles in a K8s cluster. The tutorials make use of a simple Kubernetes echoservice, which is based on a well-known K8s example.

The CNO role to install the echoservice can be found at https://gitlab.com/logius/cloud-native-overheid/components/echoservice. This role is really basic, which allows us to focus on the required steps to set up the environment.

The following tutorials are presented:

1. Run CNO role from gitlab.com, by using Ansible in `platform-runner` container
2. Run CNO role from gitlab.com, by using Ansible on host machine
3. Run CNO role from source code

Before you start, prepare a K8s test cluster with Minikube, Kind, Docker Desktop, or somewhere in the cloud.

# 1. Run CNO role from gitlab.com, using Ansible in `platform-runner` container

In this tutorial, we will run the generic ansible-playbook `./playbook/run-role.yaml`, which can deploy any CNO role. We will instruct the playbook to deploy the `echoservice` role.

The playbook is run from a docker container image `platform-runner`. This image has the required Ansible tooling prepared. For running the image it is sufficient to have a Docker runtime environment.

Steps to take:

- Get platform-runner image
- Set connection to local K8s custer
- Create custom configuration for CNO role
- Deploy CNO role via ansible playbook in platform-runner

## Get platform-runner image

Pull the platform-runner image from GitLab:
```
docker pull registry.gitlab.com/logius/cloud-native-overheid/components/platform-runner:latest
```

When encountering an authentication error, consider to logout first:
```
docker logout registry.gitlab.com
```

## Set connection to local K8s cluster

Make sure the environment variable KUBECONFIG points to a Kubectl configuration file:
```
if [ -z "$KUBECONFIG" ] || [ ! -f "$KUBECONFIG" ]; then
  echo "KUBECONFIG should refer to a K8s config file."
else
  echo OK
fi
```

Smoketest Kubectl:
```
kubectl get nodes
```

## Create custom configuration of CNO role

A custom configuration is required to deploy a role in a certain OTAP environment, such as local.

For custom configuration a YAML file is used, similar to Helm charts with a values.yml file.

Create a `custom-role-config.yml` in the working directory with the following content:
```
config:
  echoservice:
    namespace: "cno-echoservice"
```

This config overrides the echoservice role. It indicates the namespace where the echoservice component should be installed. The example is basic, later on we will see how to use this is used in advanced scenarios.

## Deploy CNO role via ansible playbook in platform-runner container

First, prepare a command for the generic playbook:
```
ANSIBLE_CMD="ansible-playbook run-role.yml \
      -e @/playbook/configfiles/custom-role-config.yml \
      -e galaxy_url=https://gitlab.com/logius/cloud-native-overheid/components/echoservice \
      -vv"; echo ${ANSIBLE_CMD}
```

This command will download and run the `echoservice` role from gitlab.com. The source code for the role is implicitly downloaded using `ansible-galaxy`. The parameter `galaxy_url` tells the playbook where to look for the code of the role.

Next, start the `platform-runner` container and run the prepared playbook command to deploy the echoservice role:
```
docker run --rm \
    -v $KUBECONFIG:/home/builder/.kube/config \
    -v $PWD/custom-role-config.yml:/playbook/configfiles/custom-role-config.yml \
    -e ANSIBLE_FORCE_COLOR=true \
    --network=host \
    registry.gitlab.com/logius/cloud-native-overheid/components/platform-runner:latest \
    $ANSIBLE_CMD
```

The echoservice role installs the K8s echoservice in your K8s cluster. Note how we mount the kubectl `$KUBECONFIG` config file and the `custom-role-config.yml` into the docker container.

The installation process should now be running, after which the platform-runner container will terminate.

## Smoke test

Validate the K8s echoservice is installed in your cluster:
```
kubectl get pods -n cno-echoservice
```

This completes the tutorial.

## Cleanup

You can now clean up the K8s cluster and remove the namespace:
```
kubectl delete ns cno-echoservice
```

# 2. Run CNO role from gitlab.com, using Ansible on host machine

In this tutorial we are not using a Docker container, but will run `ansible-playbook` directly on your host machine.

First make sure the required tooling is installed:

| Tool    | URL                                                     | Smoke test       |
| ------- | ------------------------------------------------------- | ---------------- |
| Python3 | https://www.python.org/downloads                        | python --version |
| Kubectl | https://kubernetes.io/docs/tasks/tools/install-kubectl/ | kubectl version  |
| Helm    | https://helm.sh/docs/intro/install/                     | helm version     |
| YQ      | https://github.com/mikefarah/yq                         | yq --version     |

Next, install the latest version of Ansible and required pip modules for CNO:
```
pip3 install --upgrade ansible openshift docker jmespath skopeo-bin boto3 minio s3cmd
```

Smoketest:
```
ansible --version
ansible-playbook --version
```

Checkout the GitLab project `platform-runner`, as it contains the Ansible playbook we are going to run.
```
git clone https://gitlab.com/logius/cloud-native-overheid/components/platform-runner
```

Set home directory of platformer-runner, to locate its playbooks:
```
export PLATFORM_RUNNER_HOME=$PWD/platform-runner
```

Set ENV variables to configure Ansible:
```
export ANSIBLE_DEPRECATION_WARNINGS=false
export ANSIBLE_HASH_BEHAVIOUR=merge
export ANSIBLE_HOST_PATTERN_MISMATCH=ignore
export ANSIBLE_PYTHON_INTERPRETER=auto_silent
export ANSIBLE_ROLES_PATH=~/.ansible/roles
```

The ANSIBLE_ROLES_PATH is the folder where ansible-playbook will download the CNO roles. You can change this to any other directory.

Create a `custom-role-config.yml` as in the previous tutorial (see above). Similarly, check the KUBECONFIG variable and if kubectl can correctly connect to the K8s cluster.

Finally, let ansible-playbook run the `run-role` playbook in the platform-runner directory:
```
ansible-playbook ${PLATFORM_RUNNER_HOME}/playbook/run-role.yml \
  -i localhost, --connection=local \
  -e @custom-role-config.yml \
  -e galaxy_url=https://gitlab.com/logius/cloud-native-overheid/components/echoservice \
  -vv
```

As in the first tutorial, you can now inspect the namespace for the echoservice and clean it up to conclude this tutorial.

# 3. Run CNO role from source code

This approach allows for a quick develop/test cycle when working on a role's source code.

Again you will run/test the role from the generic Ansible playbook.

Steps:

- Clone source code
- Set Ansible environment
- Deploy role
- Develop and test

## Clone source code

Clone source code for role:
```
# Create target directory
mkdir ./cno_roles

# Clone source code into target directory
git clone https://gitlab.com/logius/cloud-native-overheid/components/echoservice ./cno_roles
````

## Set Ansible environment

Set Ansible roles path to source code:
```
# Point predefined variable to source code in target directory (compare its value with tutorial 2)
export ANSIBLE_ROLES_PATH=$PWD/cno_roles
```

Prepare a simplified Ansible command that directly refers to the role's source directory (`echoservice` in this case):
```
ANSIBLE_CMD="ansible-playbook run-role.yml \
      -e @/playbook/configfiles/custom-role-config.yml \
      -e galaxy_url=echoservice \
      -vv"; echo ${ANSIBLE_CMD}
```

Note that the original Ansible command will also remain to work; the `ANSIBLE_ROLES_PATH` instructs the playbook to skip downloading the code with ansible-galaxy.

## Deploy role

Take the same steps as in the first tutorial: deploy the echoservice role via the Ansible playbook in the platformer-runner container, or deploy it directly from Ansible on the host machine:

### Deploy role from platform-runner

Use the same command as in tutorial 1.

This time the role's source code is mounted in the docker container:
```
docker run --rm \
    -v $KUBECONFIG:/home/builder/.kube/config \
    -v $PWD/custom-role-config.yml:/playbook/configfiles/custom-role-config.yml \
    -v $ANSIBLE_ROLES_PATH:/playbook/roles \
    -e ANSIBLE_FORCE_COLOR=true \
    --network=host \
    registry.gitlab.com/logius/cloud-native-overheid/components/platform-runner:latest \
    $ANSIBLE_CMD
```

### Deploy role from host machine

Use the same command as in tutorial 2.

This time, the  `galaxy_url` directly refers to the role's source code directory:
```
ansible-playbook ${PLATFORM_RUNNER_HOME}/playbook/run-role.yml \
  -i localhost, --connection=local \
  -e @custom-role-config.yml \
  -e galaxy_url=echoservice \
  -vv
```

## Develop and test

You can now make local changes to the Role and run/test it immediately, without a need to check-in first.

A good exercise is to change the version of the echoservice in `install.yml`. It is currently deploying echoserver:1.4, you might bump it up to 1.10. Save the changes, run the code again and check the results in the k8s cluster:
```
# Observe v1.10
kubectl get pod -n cno-echoservice $(kubectl get pods -n cno-echoservice | grep hello-node | awk '{print $1}') -o yaml | grep image:
```

# 4. Advanced configuration

Once you get more experienced it will become handy to use a shell script to run the code. Examples can be found in the install folder.

| Script              | Purpose                                                       |
| ------------------- | ------------------------------------------------------------- |
| cno-role-runner.sh  | Deploy a CNO role using the docker container platform-runner. |
| cno-role-ansible.sh | Deploy a CNO role using ansible-playbook directly.            |

TODO explain
- multiple config files
- merge behaviors
- patterns in config files
- platform variable
