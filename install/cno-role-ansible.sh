#!/bin/bash
# 
# Install an ansible role using ansible-playbook directly from a Linux environment. 
# Make sure kubectl, helm, ansible are installed. 
#
# Usage: 
#   cno-role-ansible.sh <role_name> <ansible args>
#   Where <role_name> is name of a role, e.g. keycloak, harbor, gitlab. 
#   The <ansible args> support all playbook options, e.g. -vv, -e, -e@. 
#
# The script uses the following environment variables:
# - KUBECONFIG (required): kubernetes config file and the current-context should be set.
# - PLATFORM_RUNNER_DIR (optional): Path to the platform-runner project. Defaults to the project containing this shell script. 
# - ROLE_URL (optional): URL of a role on gitlab.com. Defaults to https://gitlab.com/logius/cloud-native-overheid/components/<role_name>
# - ANSIBLE_ROLES_PATH (optional): Path to where the CNO roles are stored. Defaults to ~/.ansible/roles. 
# 
#
if [ $# -lt 1 ]; then 
    echo "Usage: cno-role-ansible.sh <role_name> <ansible args>"
    echo "The first arg indicates the role that needs to be installed."
    echo "The args after role_name are passed on to ansible-playbook, e.g. 'cno-role-ansible.sh echoservice -vv -e my_param=true -e @my_config.yml'"
    exit 1
fi

# Validate KUBECONFIG
if [ -z "$KUBECONFIG" ] || [ ! -f "$KUBECONFIG" ]; then
  echo "KUBECONFIG should refer to a K8s config file."
  exit 1
fi

# ROLE_URL is the url of the gitlab project containing the CNO role. 
if [ -z ${ROLE_URL} ]; then
	ROLE_URL=https://gitlab.com/logius/cloud-native-overheid/components/$1
fi

# PLATFORM_RUNNER_DIR is the root of the project platform-runner. 
if [ -z ${PLATFORM_RUNNER_DIR} ]; then
	PLATFORM_RUNNER_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && cd .. && pwd )"
  echo "Running with PLATFORM_RUNNER_DIR=$PLATFORM_RUNNER_DIR"
fi

# ANSIBLE_ROLES_PATH is the directory where the CNO roles are downloaded.
if [ -z ${ANSIBLE_ROLES_PATH} ]; then
	ANSIBLE_ROLES_PATH="~/.ansible/roles"
fi

export ANSIBLE_HASH_BEHAVIOUR=merge
export ANSIBLE_HOST_PATTERN_MISMATCH=ignore
export ANSIBLE_PYTHON_INTERPRETER=auto_silent

echo "==========================================================================="
echo "Running:"
echo "   role:         $1"
echo "   url:          $ROLE_URL"
echo "   roles_path:   $ANSIBLE_ROLES_PATH"
echo "   kubeconfig:   $KUBECONFIG"
echo "   workdir:      $PWD"
echo "   ansible args: ${@:2}"
echo "==========================================================================="

# The remaining command line args are simply passed on to ansible-playbook. 
ansible-playbook $PLATFORM_RUNNER_DIR/playbook/run-role.yml \
  -i localhost, --connection=local \
  -e galaxy_url=$ROLE_URL \
  ${@:2}

-
