#
# Purpose:
#  Install a CNO role using ansible-playbook in a docker container.
#  Mounts are created in the Docker container for the roles, config files and the kubeconfig file.
#
# Usage:
#   cno-role-runner.sh <role_name> <ansible args>
#   where <role_name> is name of a role, e.g. keycloak, harbor, gitlab.
#
# The script uses the following environment variables:
# - KUBECONFIG (required): kubernetes config file and the current-context should be set.
# - ROLE_URL (optional): URL of a role on gitlab.com. Defaults to https://gitlab.com/logius/cloud-native-overheid/components/<role_name>
# - ANSIBLE_ROLES_PATH (optional): Path to where the CNO roles are stored. Defaults to ~/.ansible/roles.
# - CNO_CONFIG_FILES (optional): Path to one or more custom config files, separated by semicolon. Defaults to $PWD/custom_config.yml.
# - TOOLBOX_TAG (optional): tag of the toolbox (e.g. 'local', 'latest', or v9.0.0). Default is 'latest'.
#
if [ $# -lt 1 ]; then
    echo "Usage: cno-role-runner.sh <role_name> <ansible args>"
    echo "The first arg indicates the role that needs to be installed."
    echo "The args after role_name are passed on to ansible-playbook, e.g. 'cno-role-runner.sh echoservice -vv -e my_param=true'"
    exit 1
fi

if [ -z "$TOOLBOX_TAG" ]; then
  TOOLBOX_TAG=latest
fi
TOOLBOX_IMG=registry.gitlab.com/logius/cloud-native-overheid/components/platform-runner:${TOOLBOX_TAG}

# Pull the image
docker pull $TOOLBOX_IMG

# Validate KUBECONFIG
if [ -z "$KUBECONFIG" ] || [ ! -f "$KUBECONFIG" ]; then
  echo "KUBECONFIG should refer to a K8s config file."
  exit 1
fi

# ROLE_URL is the url of the gitlab project containing the CNO role.
if [ -z ${ROLE_URL} ]; then
	ROLE_URL=https://gitlab.com/logius/cloud-native-overheid/components/$1
fi

# ANSIBLE_ROLES_PATH is the directory where the CNO roles are downloaded.
if [ -z ${ANSIBLE_ROLES_PATH} ]; then
	ANSIBLE_ROLES_PATH="~/.ansible/roles"
fi

# Validate ANSIBLE_ROLES_PATH
if [ -n "$ANSIBLE_ROLES_PATH" ]; then
  if [ ! -d "$ANSIBLE_ROLES_PATH" ]; then
    echo "The directory \"$ANSIBLE_ROLES_PATH\" was provided as path to the roles, but it does not exist."
    exit 1
  fi
fi

# Prepare mounts for config files
CONFIG_FILES_ARRAY=$(echo $CNO_CONFIG_FILES | tr ";" "\n")
for CONFIG_FILE in $CONFIG_FILES_ARRAY
do
  # Test if config files exist
  if [ ! -f "$CONFIG_FILE" ]; then
    echo "Configuration file <$CONFIG_FILE> not found."
    exit 2
  fi

  # Add the configfile as a mount for Docker container
  CONFIG_FILES_MOUNTS="$CONFIG_FILES_MOUNTS -v $CONFIG_FILE:/playbook/configfiles/`basename $CONFIG_FILE`"
  # Provide config file to ansible playbook with '-e @' parameter
  CONFIG_FILES_PARAM="$CONFIG_FILES_PARAM -e @/playbook/configfiles/`basename $CONFIG_FILE`"
done

# Ansible-playbook command.
# The remaining command line args are simply passed on to ansible-playbook.
ANSIBLE_CMD="ansible-playbook /playbook/run-role.yml \
  $CONFIG_FILES_PARAM \
  -e galaxy_url=$ROLE_URL \
  ${@:2}"

echo "==========================================================================="
echo "Running:"
echo "   role:       $1"
echo "   url:        $ROLE_URL"
echo "   roles_path: $ANSIBLE_ROLES_PATH"
echo "   kubeconfig: $KUBECONFIG"
echo "   workdir:    $PWD"
echo "   image:      $TOOLBOX_IMG"
echo "   ansible:    $ANSIBLE_CMD"
echo "==========================================================================="

docker run --rm \
  -v $KUBECONFIG:/home/builder/.kube/config \
  -v $ANSIBLE_ROLES_PATH:/playbook/roles \
  $CONFIG_FILES_MOUNTS \
  --network=host \
  $TOOLBOX_IMG $ANSIBLE_CMD


