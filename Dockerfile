####
#### Download tools
#### 

FROM alpine AS downloader

RUN apk add --no-cache wget

# Put the version also in versions.yml, for use by release-info check.

# Helm: https://github.com/helm/helm/releases
ENV HELM3_VERSION="v3.12.1"
# Kubectl: https://github.com/kubernetes/kubectl/releases
ENV KUBE_VERSION="v1.25.11"
# YQ: https://github.com/mikefarah/yq/releases
ENV YQ_VERSION="v4.34.1"
# Kubeseal: https://github.com/bitnami-labs/sealed-secrets
ENV KUBESEAL_VERSION="0.22.0"
# Kustomize: https://github.com/kubernetes-sigs
ENV KUSTOMIZE_VERSION="v4.5.7"

RUN mkdir -p /downloader/bin

# Kubectl
RUN wget -q https://storage.googleapis.com/kubernetes-release/release/${KUBE_VERSION}/bin/linux/amd64/kubectl -O /downloader/bin/kubectl 

# Helm
RUN wget -q https://get.helm.sh/helm-${HELM3_VERSION}-linux-amd64.tar.gz -O - | tar -xzO linux-amd64/helm > /downloader/bin/helm 

# Yq (pip yq is too old)
RUN wget -q https://github.com/mikefarah/yq/releases/download/${YQ_VERSION}/yq_linux_amd64 -O /downloader/bin/yq 

# Kubeseal
RUN wget -q https://github.com/bitnami-labs/sealed-secrets/releases/download/v${KUBESEAL_VERSION}/kubeseal-${KUBESEAL_VERSION}-linux-amd64.tar.gz  -O - | tar -xzO kubeseal > /downloader/bin/kubeseal 

# Kustomize 
RUN wget -q https://github.com/kubernetes-sigs/kustomize/releases/download/kustomize%2F${KUSTOMIZE_VERSION}/kustomize_${KUSTOMIZE_VERSION}_linux_amd64.tar.gz -O - | tar -xzO kustomize > /downloader/bin/kustomize 

RUN chmod +x /downloader/bin/* 

####
#### Runner image based on python and ansible 
#### 

FROM python:3.10-slim

LABEL maintainer "Team CNO"

# Ansible: https://github.com/ansible-community/ansible-build-data/blob/main/5/CHANGELOG-v5.rst
# Put the version also in versions.yml, for use by release-info check.
ENV ANSIBLE_VERSION="v6.1.0" 
# CNO Ansible modules https://gitlab.com/logius/cloud-native-overheid/components/ansible-modules
ENV CNO_ANSIBLE_MODULES_VERSION="2.0.0"

# OS tools and core dependencies.
RUN apt-get update && apt-get upgrade -y \
  && apt-get install -y --no-install-recommends gnupg lsb-release curl vim git wget openssh-client jq gettext
# Add apt vault repo and install vault packge

RUN wget -O- https://apt.releases.hashicorp.com/gpg | gpg --dearmor -o /usr/share/keyrings/hashicorp-archive-keyring.gpg \
  && echo "deb [signed-by=/usr/share/keyrings/hashicorp-archive-keyring.gpg] https://apt.releases.hashicorp.com $(lsb_release -cs) main" | tee /etc/apt/sources.list.d/hashicorp.list \
  && apt update && apt install vault -y --no-install-recommends \
  && apt-get clean && rm -rf /var/lib/apt/lists/* && rm -rf /usr/share/man/

COPY --from=downloader /downloader/bin/* /usr/local/bin/

# Copy Playbook
COPY playbook /playbook/

# Copy Ansible python modules 
RUN git clone -c advice.detachedHead=false --depth 1 -b $CNO_ANSIBLE_MODULES_VERSION https://gitlab.com/logius/cloud-native-overheid/components/ansible-modules.git /playbook/modules

# Playbook initialization
RUN mkdir -m 777 /temp-install \
  && mkdir -m 777 /playbook/roles

WORKDIR /playbook

# Merge behavior for hashmaps https://docs.ansible.com/ansible/latest/reference_appendices/config.html#default-hash-behaviour
ENV ANSIBLE_HASH_BEHAVIOUR=merge
# Ignore incomplete inventory file
ENV ANSIBLE_HOST_PATTERN_MISMATCH=ignore
# Inventory with local host
ENV ANSIBLE_INVENTORY=/playbook/inventory
# Location of custom modules
ENV ANSIBLE_LIBRARY="$ANSIBLE_ROLES_PATH/common/modules:/playbook/modules"
# Use the default python
ENV ANSIBLE_PYTHON_INTERPRETER=auto_silent
# Location of roles
ENV ANSIBLE_ROLES_PATH=/playbook/roles
# Provide readable error message
ENV ANSIBLE_STDOUT_CALLBACK=debug

# Add non root user
RUN useradd -ms /bin/bash builder && chown builder:builder /home/builder && chmod 755 /home/builder

USER builder

# Create .kube directory as builder
RUN mkdir -p /home/builder/.kube

CMD [ "ansible-playbook", "--version" ]
